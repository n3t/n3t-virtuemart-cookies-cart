<?php
/**
 * @package n3t Virtuemart Cookies Cart
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2021 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

class VirtueMartCart extends n3tVMCookiesCartVirtueMartCart {

  public function updateProductCart() {
    parent::updateProductCart();
    $dispatcher = JDispatcher::getInstance();
    $dispatcher->trigger('plgVmOnCartUpdated', [$this]);
  }

}
