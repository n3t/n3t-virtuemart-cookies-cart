n3t Virtuemart Cookies Cart
===========================

n3t Virtuemart Cookies Cart Joomla! plugin allows to store visitors cart content in cookies.
This will keep their cart contents even after session expires, or the browser is closed.

Installation
------------

n3t Virtuemart Cookies Cart is Joomla! plugin. It could be installed as any other extension in
Joomla!

After installing **do not forget to enable the plugin** from Plugin Manager in your
Joomla! installation.

Configuration
-------------

Go to plugin settings and set how long should be cart stored (defaults to 1 year).
