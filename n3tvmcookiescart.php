<?php
/**
 * @package n3t Virtuemart Cookies Cart
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2021 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\CMS\Plugin\CMSPlugin;

class plgSystemN3tVmCookiesCart extends CMSPlugin
{

  protected function overrideClass($class, $file)
  {
    jimport('joomla.filesystem.file');
    if (JFile::exists(JPATH_ROOT . $file)) {
      $content = JFile::read(JPATH_ROOT . $file);
      $content = str_replace('<?php', '', $content);
      $content = preg_replace('~class\s+' . $class . '~i', 'class n3tVMCookiesCart' . $class, $content, 1);
      eval($content);
      require_once (__DIR__ . '/overrides/' . $class . '.php');
    }
  }

  private function clearCartCookie()
  {
    $app = JFactory::getApplication();
    $app->input->cookie->set('vmcart', '', time() - 1, $app->get('cookie_path', '/'), $app->get('cookie_domain'), $app->isSSLConnection());
  }

  private function storeCartCookie($cart, $removeId = null)
  {
    $user = JFactory::getUser();
    if ($user->id)
      return;

    $products = [];
    foreach ($cart->cartProductsData as $index => $cartProduct) {
      if ($removeId === $index)
        continue;

      $product = [
        'id' => $cartProduct['virtuemart_product_id'],
        'qty' => $cartProduct['quantity'],
      ];

      if ($cartProduct['customProductData'])
        $product['custom'] = $cartProduct['customProductData'];

      $products[] = $product;
    }

    $app = JFactory::getApplication();
    if ($products) {
      $products = array_slice($products, 0, $this->params->get('products_limit', 20));
      $value = json_encode($products);
      $time = time() + $this->params->get('cookie_lifetime', 365) * 60 * 60 * 24;
    } else {
      $value = '';
      $time = time() - 1;
    }

    $app->input->cookie->set('vmcart', $value, $time, $app->get('cookie_path', '/'), $app->get('cookie_domain'), $app->isSSLConnection());
  }

  public function plgVmOnAddToCart(&$cart)
  {
    $this->storeCartCookie($cart);
  }

  public function plgVmOnRemoveFromCart(&$cart, $productId)
  {
    $this->storeCartCookie($cart, $productId);
  }

  public function plgVmOnCartUpdated(&$cart)
  {
    $this->storeCartCookie($cart);
  }

  public function plgVmOnUserOrder(&$order)
  {
    $this->clearCartCookie();
  }

  public function onAfterInitialise()
  {
    $app = JFactory::getApplication();
    if (
         $app->isClient('site')
      && $app->input->get('option') == 'com_virtuemart'
    ) {
      $this->overrideClass('VirtueMartCart', '/components/com_virtuemart/helpers/cart.php');
    }
  }

  public function onAfterRoute()
  {
    $app = JFactory::getApplication();
    if ($app->isClient('administrator'))
      return;

    $user = JFactory::getUser();
    if ($user->id) {
      $products = $app->input->cookie->get('vmcart', null, 'raw');
      if ($products)
        $this->clearCartCookie();
      return;
    }


    if (!class_exists( 'VmConfig' ))
      require(JPATH_ROOT .'/administrator/components/com_virtuemart/helpers/config.php');
    VmConfig::loadConfig();

    $cart = VirtueMartCart::getCart();
    if (!$cart)
      return;

    if ($cart->cartProductsData)
      return;

    $products = $app->input->cookie->get('vmcart', null, 'raw');

    if (!$products)
      return;

    try {
      $products = json_decode($products);
    } catch (\Exception  $e) {
      return;
    }

    $ids = [];
    foreach ($products as $product) {
      $ids[] = (int)$product->id;
      $qty[] = (int)$product->qty;
      if (isset($product->custom))
        $custom[] = $product->custom;
      else
        $custom[] = [];
    }

    $_REQUEST['quantity'] = $qty;
    $_REQUEST['customProductData'] = $custom;
    $cart->add($ids, $error);
  }
}